using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using models;

namespace tech_test_payment_api.models
{
    public class Venda
    {
        [Key]
        public int IdVenda { get; set; }
        public DateTime Data { get; set; }
        public string Status { get; set; }

        [ForeignKey("Vendedor")]
        public int VendedorIdVendedor { get; set; }
        public Vendedor Vendedor { get; set; }

        [ForeignKey("Produto")]
        public int ProdutoIdProduto { get; set; }
        public Produto Produto { get; set; }

        public Venda(int vendedorIdVendedor, int produtoIdProduto)
        {
            Data = DateTime.Now;
            Status = "Aguardando Pagamento";
            VendedorIdVendedor = vendedorIdVendedor;
            ProdutoIdProduto = produtoIdProduto;
        }
    }
}