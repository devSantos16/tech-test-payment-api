using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using models;
using tech_test_payment_api.models;
using Newtonsoft.Json;
using Context;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly PaymentContext context;

        public VendaController(PaymentContext context)
        {
            this.context = context;
        }

        [HttpPost("RegistrarVendedor")]
        public ActionResult RegistrarVendedor(Vendedor v)
        {
            var bd = this.context.Vendedores.Add(v);
            this.context.SaveChanges();

            return Ok($"Vendedor {v.Nome} cadastrado com sucesso!");
        }

        [HttpGet("MostrarVendedores")]
        public ActionResult MostrarVendedores()
        {
            return Ok(this.context.Vendedores.ToList());
        }

        [HttpPost("RegistrarProduto")]
        public ActionResult CadastrarProdutos(Produto p)
        {
            var bd = this.context.Produtos.Add(p);
            this.context.SaveChanges();

            return Ok($"Produto {p.Nome} cadastrado com sucesso!");
        }

        [HttpPost("RegistrarVenda")]
        public ActionResult RegistrarVenda(int produtoIdProduto, int vendedorIdVendedor)
        {
            Venda v = new Venda(vendedorIdVendedor, produtoIdProduto);
            var venda = this.context.Vendas.Add(v);
            this.context.SaveChanges();

            return Ok();
        }
        
        [HttpPut("AtualizarVenda")]
        public ActionResult AtualizarVenda(int idVenda, bool opcao)
        {
            var venda = this.context.Vendas.Find(idVenda);

            if(opcao == false){
                venda.Status = "Cancelado";
                this.context.SaveChanges();
                return Ok("Produto cancelado com sucesso");
            }

            switch (venda.Status){
                case "Aguardando Pagamento":
                    venda.Status = "Enviado para transportadora";
                    break;
                case "Enviado para transportadora":
                    venda.Status = "Entregue";
                    break;
                case "Entregue":
                    return Ok("O item ja foi entregue");
                case "Cancelado":
                    return Ok("O produto foi cancelado e não poderá ser atualizado novamente");
                default:
                    return NotFound();
            }

            this.context.SaveChanges();
            return Ok("Status atualizado com sucesso");
        }

        [HttpGet("BuscarVenda")]
        public ActionResult BuscarVenda(int idVenda)
        {
            var venda = this.context.Vendas.Find(idVenda);
            var produto = this.context.Produtos.Where(x => x.IdProduto == venda.ProdutoIdProduto);
            var vendedor = this.context.Vendedores.Where(x => x.IdVendedor == venda.VendedorIdVendedor);

            return Ok(new { venda, produto, vendedor });
        }

        [HttpGet("MostrarProdutos")]
        public ActionResult MostrarProdutos()
        {
            return Ok(this.context.Produtos.ToList());
        }
    }
}